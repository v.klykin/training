var cityValue = document.getElementById('city');
var listDiv = document.getElementsByClassName('list');
var mapDiv = document.getElementsByClassName('map');
var count = 1;
var houses = [];
var listings,latitude,longitude;
var housesJson = localStorage.getItem('houses');
var map;
if (housesJson !== null) {
  houses = JSON.parse(housesJson);
}

var src = "https://api.nestoria.co.uk/api?encoding=json&pretty=1&action=search_listings&country=uk&place_name=";



function addScript(event) {
  if (event.keyCode != 13) {
    return;
  }
  if (cityValue.value === '') {
    return;
  }
  listDiv[0].innerHTML = '';
  if (map !== undefined) {
    map.destroy();
  }
  count = 1;
  var script = document.createElement('script');
  script.src = src + cityValue.value + '&page=' + count + '&callback=listingsHouse';
  document.head.appendChild(script);

}



function addScriptNewPage () {
  var script = document.createElement('script');
  script.src = src + cityValue.value + '&page=' + count + '&callback=listingsHouse';
  document.head.appendChild(script);
  listDiv[0].removeEventListener('click', showModal)
}

cityValue.addEventListener('keyup', addScript);


function listingsHouse(date) {
 
  listings = date.response.listings;
  if (listings.length === 0) {
    return;
  }
  if (listDiv[0].children.length !== 0) {
    listDiv[0].removeChild(listDiv[0].lastChild);
  }

  console.log(listings);
  for (let x = 0; x < listings.length; x++) {

    var listingDiv = document.createElement('div');
    listingDiv.style.width = '450px';
    listingDiv.style.height = '100px';
    listingDiv.style.marginTop = '20px';
    listingDiv.style.borderRadius = '10px';
    listingDiv.style.backgroundColor = 'lightblue';
    listingDiv.style.padding = '10px';
    listingDiv.id = x;
    listingDiv.className = 'show';
    listingDiv.style.cursor = 'pointer';
    listingDiv.style.marginLeft = 'auto';
    listingDiv.style.marginRight = 'auto';
    var img = document.createElement('img');
    img.style.cursor = 'default';
    img.src = listings[x].thumb_url;
    img.style.margin = '5px 5px';
    img.align = 'right';
    listingDiv.appendChild(img);
    var h4 = document.createElement('h4');
    h4.style.align = 'left';
    h4.style.margin = '0px';
    var price = document.createTextNode(listings[x].price_formatted);
    h4.appendChild(price);
    listingDiv.appendChild(h4);

    var br = document.createElement('br');
    listingDiv.appendChild(br);
    var span = document.createElement('span');
    span.style.color = 'grey';
    span.style.cursor = 'default';
    span.style.marginTop = '10px';
    var summary = document.createTextNode(listings[x].summary);
    span.appendChild(summary);
    listingDiv.appendChild(span);

    var buttonMap = document.createElement('button');
    buttonMap.innerText = 'Показать на карте';
    buttonMap.style.float = 'right';
    buttonMap.style.position ='relative';
    buttonMap.style.left = '70px';
    buttonMap.style.top = '20px';
    listingDiv.appendChild(buttonMap);

    listDiv[0].appendChild(listingDiv);
    buttonMap.addEventListener('click', () => {addPoint(listings[x])});
  }


 

  var br = document.createElement('br')
  listDiv[0].appendChild(br);

  var buttonElse = document.createElement('button');
  buttonElse.textContent = 'Показать ещё';
  buttonElse.style.display= 'block';
  buttonElse.style.marginLeft = 'auto';
  buttonElse.style.marginRight = 'auto';
  buttonElse.style.borderRadius = '50px';
  buttonElse.style.backgroundColor = 'lightblue';
  listDiv[0].appendChild(buttonElse);

  if (count === 1){
    ymaps.ready(init);
      function init(){ 
        map = new ymaps.Map(mapDiv[0],{
        center: [listings[0].latitude,listings[0].longitude],
        zoom: 10
        }); 
    }
  }
  
  

  buttonElse.addEventListener('click', addHouse);
  
  listDiv[0].addEventListener('click', showModal)
}

function addPoint (house) {
  var myPlacemark = new ymaps.Placemark([house.latitude, house.longitude], {
                                          iconContent: house.price_formatted,
                                          hintContent: house.summary
                                        } , {
                                          preset: 'islands#blueStretchyIcon'
                                        });

 
  map.geoObjects.add(myPlacemark);
}

function showModal (event) {
  console.log(1);
  if (event.target.className.toLowerCase() === 'show') {
    
    var modal = document.getElementById('modal');
    modal.style.display = 'block';

    var modalContent = document.getElementById('modal-content');
    var id = event.target.id;
    var divInfo = document.createElement('div');
    divInfo.style.float = 'left';
    if (listings[id].bathroom_number != 0) {
      var modalBathroom = document.createTextNode('Количество ванн комнат: ' + listings[id].bathroom_number);
      divInfo.appendChild(modalBathroom);
      var br = document.createElement('br');
      divInfo.appendChild(br);
    }

    if (listings[id].bedroom_number != 0) {
      var modalBedroom = document.createTextNode('Количество спален: ' + listings[id].bedroom_number);
      divInfo.appendChild(modalBedroom);
      var br = document.createElement('br');
      divInfo.appendChild(br);
    }
    
    var modalPrice = document.createTextNode('Цена: ' + listings[id].price_formatted);
    divInfo.appendChild(modalPrice);
    modalContent.appendChild(divInfo);

    var addMarks = document.createElement('button');
    addMarks.innerText = 'Добавить в закладки';
    addMarks.style.float = 'left';
    addMarks.style.marginTop = '25%';
    addMarks.style.marginLeft = '-15%';
    modalContent.appendChild(addMarks);

    addMarks.addEventListener('click', () => {addMarksInHouse(id, listings)})

    var divImg = document.createElement('div');
    divImg.style.float = 'right';
    var imgModal = document.createElement('img');
    imgModal.src = listings[id].img_url;
    divImg.appendChild(imgModal);
    modalContent.appendChild(divImg);

    
  }
}

function addMarksInHouse (id,listings) {
  houses = localStorage.getItem('houses');
  houses = JSON.parse(houses);
  houses.forEach(function (item) {
    if (item.lister_url === listings[id].lister_url) {
     listings = [];
    }
  })
  if (listings.length === 0) {
    return;
  }
  houses.push(listings[id]);
  var housesJson = JSON.stringify(houses);
  localStorage.setItem('houses', housesJson);
}

document.addEventListener('click', (event) => {
  if (event.target.id === 'modal') {
    notShowModal();
  }
})

function notShowModal () {
  var modal = document.getElementById('modal');
  modal.style.display = 'none';
  var modalContent = document.getElementById('modal-content');
  modalContent.innerHTML = '';
}

function addHouse() {
  // var listingDiv = document.getElementById('listings');
  // document.body.removeChild(listingDiv);
  count++
  addScriptNewPage();
}

var buttonMark = document.getElementById('windowMark');
buttonMark.addEventListener('click', showModalMarks);

function showModalMarks () {
  var modalMark = document.getElementById('modal-mark');
  modalMark.style.display = 'block';
  var modalContentMark = document.getElementById('modal-content-mark');
  houses.forEach(function (item, i) {
      var div = document.createElement('div');
      div.style.cursor = 'pointer'
      div.id = i;
      div.style.background = 'lightgrey';
      div.style.margin = '5px';
      div.className = 'show';

      var img = document.createElement('img');
      img.src = item.thumb_url;
      img.style.cursor = 'default';

      var buttonDel = document.createElement('button');
      buttonDel.innerText = 'Удалить';
      buttonDel.style.float = 'right';
      buttonDel.style.borderRadius = '5px';
      buttonDel.style.marginTop = '20px';
      div.appendChild(buttonDel);
      div.appendChild(img);

      var divText = document.createElement('div');
      divText.style.display = 'inline-block';
      divText.style.position = 'relative';
      divText.style.bottom = '20px';
      divText.style.left = '10px';
      divText.style.cursor = 'default';
      var text = document.createTextNode(' ' + item.price_formatted)
      divText.appendChild(text);
      div.appendChild(divText);

      modalContentMark.appendChild(div);

      buttonDel.addEventListener('click',() => {delMark(item, div, modalContentMark)});
  })
  
  

  modalContentMark.addEventListener('click', showModalFullMarks)
  
}
function delMark (house, div, modalContentMark) {
  var housesMarks = localStorage.getItem('houses');
  housesMarks = JSON.parse(housesMarks);
  housesMarks.forEach(function (item, i) {
    if (house.lister_url === item.lister_url) {
    modalContentMark.removeChild(div);
    housesMarks.splice(i, 1);
    }
  })
  housesMarks = JSON.stringify(housesMarks);
  localStorage.setItem('houses', housesMarks);
}

function showModalFullMarks (event) {
  if (event.target.tagName.toLowerCase() === 'button') {
    return;
  }
  console.log(event.target);
  
  if (event.target.className === 'show') {
    
    var modal = document.getElementById('modal');
    modal.style.display = 'block';

    var modalContent = document.getElementById('modal-content');
    var id = event.target.id;
    console.log(event.target)
    var divInfo = document.createElement('div');
    divInfo.style.float = 'left';
    if (houses[id].bathroom_number != 0) {
      var modalBathroom = document.createTextNode('Количество ванн комнат: ' + houses[id].bathroom_number);
      divInfo.appendChild(modalBathroom);
      var br = document.createElement('br');
      divInfo.appendChild(br);
    }

    if (houses[id].bedroom_number != 0) {
      var modalBedroom = document.createTextNode('Количество спален: ' + houses[id].bedroom_number);
      divInfo.appendChild(modalBedroom);
      var br = document.createElement('br');
      divInfo.appendChild(br);
    }
    
    var modalPrice = document.createTextNode('Цена: ' + houses[id].price_formatted);
    divInfo.appendChild(modalPrice);
    modalContent.appendChild(divInfo);

    var divImg = document.createElement('div');
    divImg.style.float = 'right';
    var imgModal = document.createElement('img');
    imgModal.src = houses[id].img_url;
    divImg.appendChild(imgModal);
    modalContent.appendChild(divImg);

    
  }
  
}

document.addEventListener('click', (event) => {
  if (event.target.id === 'modal-mark') {
    notShowModalMarks();
  }
})

function notShowModalMarks () {
  var modalMark = document.getElementById('modal-mark');
  modalMark.style.display = 'none';
  var modalContentMark = document.getElementById('modal-content-mark');
  modalContentMark.innerHTML = '';
}
